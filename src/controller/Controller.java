package controller;

import view.RefillMachineFrame;
import model.RefillCard;

public class Controller {
	
	private RefillCard card;
	private RefillMachineFrame frame;
	
	/**
    	������� objects ��С�÷ӧҹ�ͧ���ʵ�ҧ� �����������ǡѹ(controller)���������µ�͡�����¡��ҹ���������º
	 */
	public Controller() {
		card = new RefillCard();
		frame = new RefillMachineFrame();
	}
	
	public RefillCard getRefillCard() {
		return card;
	}
	
	public RefillMachineFrame getRefillMachineFrame() {
		return frame;
	}
	
}
