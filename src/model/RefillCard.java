package model;

public class RefillCard {

	private double balance;

	/**
      Constructs a card with a zero balance.
	 */
	public RefillCard() {
		this.balance = 0;
	}

	public RefillCard(double amount) {
		this.balance = amount;
	}

	public void buy(double amount) {
		assert amount <= this.balance && amount > 0;
		if(amount <= this.balance && amount > 0) {
			this.balance -= amount;
		}
	}

	public void refill(double amount) {
		assert amount >= 0;
		if(amount >= 0) {
			this.balance += amount;
		}
	}

	public double getBalance() {
		return this.balance;
	}

}
